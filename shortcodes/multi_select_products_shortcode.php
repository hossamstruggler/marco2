<?php 

if (! class_exists( 'WPCAREME_Multi_Select_Products_ShortCode' )) {
	/**
	 * summary
	 */
	class WPCAREME_Multi_Select_Products_ShortCode {
	    /**
	     * summary
	     */
	    public function __construct()
	    {
	        add_shortcode('wpcareme_wcmsp', array( $this, 'wpcareme_multi_select_products' ));
	    }

	    public function wpcareme_multi_select_products($atts = null, $content = null)
	    {
	    	extract(shortcode_atts( array(
	    		"uniqid" => uniqid(),
				"custom_class" => "",
				"inc_cats" => "",
				"exc_cats" => "",
				"columns" => "3",
				"products_perpage" => -1,
				"order" => 'DESC',
				"orderby" => 'date',
				"meta_key" => "",
				"meta_value" => "",
				"min_no" => "3",
				"quickview" => 'yes',
				"linkto" => 'cart',
				"show_excerpt" => 'yes',
				"excerpt_length" => '30',
				"img_size" => 'full',
				"show_price" => 'yes',
				"price_background_color" => '',
				"btn_background_color" => '',
				"btn_font_size" => '',
				"btn_font_color" => '',
				"btn_padding" => '',
				"btn_margin" => '',
				"btn_hover_background_color" => '',
				"btn_hover_font_size" => '',
				"btn_hover_font_color" => '',
				"btn_hover_padding" => '',
				"btn_hover_margin" => '',
				"info_background_color" => '',
				"info_font_size" => '',
				"info_font_color" => '',
				"info_padding" => '',
				"info_margin" => ''
			), $atts ));
	    	

	    	$content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
	    	
	    	wp_register_style("wpcareme-wcmsp-main.css", plugins_url("../modules/assets/css/main.css", __FILE__));
	    	wp_enqueue_style("wpcareme-wcmsp-main.css");

	    	/// Explode categories
			if ( isset( $atts['inc_cats'] ) ) {
				$inc_cats = explode( ',', $atts['inc_cats'] );
				$inc_cats = array_map( 'trim', $inc_cats );

			} else {
				$inc_cats = '';
			}

			$args = array(
			    'post_type' => 'product',
			    'post_status' => 'publish',
			    'posts_per_page' => $products_perpage,
	      		'orderby' =>$orderby,
		    	'order' =>$order,
		    	'meta_key' => $meta_key,
		    	'meta_value' => $meta_value
			);

			if($inc_cats != '') {
				$args['tax_query'] = array(
					array(
						array(
							'taxonomy' => 'product_cat',
							'field'    => 'id',
							'terms'    => $inc_cats
						),
					)
				);
			}


			$price_background_color = ($price_background_color != '') ? "background-color: {$price_background_color};" : "";
			
			//info
			$btn_background_color_info = ($btn_background_color != '') ? "background-color: {$btn_background_color}!important;" : "";
			$btn_font_color_info = ($btn_font_color != '') ? "color: {$btn_font_color}!important;" : "";
			$btn_font_size_info = ($btn_font_size != '') ? "font-size: {$btn_font_size}px!important;" : "";
			$btn_margin_info = ($btn_margin != '') ? "margin: {$btn_margin}px!important;" : "";
			$btn_padding_info = ($btn_padding != '') ? "padding: {$btn_padding}px!important;" : "";
			
			//btn
			$btn_background_color = ($btn_background_color != '') ? "background-color: {$btn_background_color};" : "";
			$btn_font_color = ($btn_font_color != '') ? "color: {$btn_font_color};" : "";
			$btn_font_size = ($btn_font_size != '') ? "font-size: {$btn_font_size}px;" : "";
			$btn_margin = ($btn_margin != '') ? "margin: {$btn_margin}px;" : "";
			$btn_padding = ($btn_padding != '') ? "padding: {$btn_padding}px;" : "";

			//btn hover
			$btn_hover_background_color = ($btn_hover_background_color != '') ? "background-color: {$btn_hover_background_color}!important;" : "";
			$btn_hover_font_color = ($btn_hover_font_color != '') ? "color: {$btn_hover_font_color}!important;" : "";
			$btn_hover_font_size = ($btn_hover_font_size != '') ? "font-size: {$btn_hover_font_size}px!important;" : "";
			$btn_hover_margin = ($btn_hover_margin != '') ? "margin: {$btn_hover_margin}px!important;" : "";
			$btn_hover_padding = ($btn_hover_padding != '') ? "padding: {$btn_hover_padding}px!important;" : "";
			
			//Info Box
			$info_background_color = ($info_background_color != '') ? "background-color: {$info_background_color}!important;" : "";
			$info_font_color = ($info_font_color != '') ? "color: {$info_font_color}!important;" : "";
			$info_font_size = ($info_font_size != '') ? "font-size: {$info_font_size}px!important;" : "";
			$info_margin = ($info_margin != '') ? "margin: {$info_margin}px!important;" : "";
			$info_padding = ($info_padding != '') ? "padding: {$info_padding}px!important;" : "";

			
			
			$products_query = new WP_Query( $args );

			$output = '';
			if ($products_query->have_posts()) {

				$output .= '<div id="'.$uniqid.'" class="row small-up-1 large-up-'.$columns.' '.$custom_class.'">';
				while ($products_query->have_posts()) : 

					$products_query->the_post();

					$current_id = get_the_ID();

					$product = wc_get_product( $current_id );
					
					
					$product_variations = new WC_Product_Variable( $current_id );
					$variations = $product_variations->get_available_variations();
					$attributes = $product_variations->get_variation_attributes();
					
					if (!$product->is_type( 'grouped' )) {

						if ($product->is_type( 'composite' )) {
							$is_composite = 'is_composite';
						}else{
							$is_composite = '';
						}

					    $output .= '<div class="column tt-wrapper">';
					    	if($product->is_type( 'simple' )) {
								$addtocart_title = __('Add To Cart', 'wc-multi-select-product');
							}else{
								$addtocart_title = __('Select Options', 'wc-multi-select-product');
							}
					        $output .='<div class="wpcare-woocommerce-product-wrap">
					          <div class="wpcare-woocommerce-product-image">';
					          	if (get_the_post_thumbnail_url($current_id)) {
					          		$output .= '<img class="thumbnail" src="'.get_the_post_thumbnail_url($current_id, $img_size).'">';
					          	}else {
					          		$output .= '<img class="thumbnail" src="http://placehold.it/400x400">';
					          	}
					          	if ($show_price == 'yes') {
					            	$output .= '<div class="wpcareme-wcmsp-price" style="'.$price_background_color.'">'.$product->get_price_html().'</div>';
					          	}
					          $output .='</div>
					          <div class="wpcare-woocommerce-product-title">
					            <h5>'. get_the_title() .'</h5>
					          </div>';
					          	if($show_excerpt == 'yes') {
						          	$output .='<div class="wpcare-woocommerce-product-description">
						            	'.wp_trim_words( get_the_content(), $excerpt_length, '' ).'
						          	</div>';
						      	}
					          	if ($variations && !$product->is_type( 'composite' )) {
									$variationIds = array();
									foreach ($variations as $key => $variation) {

		                                array_push($variationIds, $variation['variation_id']);

									}
									$index = 0;
									$output .= "<form class='variations_form cart' method='post' enctype='multipart/form-data' data-product_id='{$current_id}' data-product_variations='".json_encode($variations)."'>";

										$output .= '<table class="variations">
										<tbody>';

											
											foreach ($attributes as $attribute_name => $options) {

												
												//$meta = get_post_meta($variationIds[$index], 'attribute_'.$attribute_name, true);
												
												$output .= '<tr>';
												$output .= '<td class="label">' . wc_attribute_label( $attribute_name ) . '</td>';
												$output .= '<td class="value">';
												$terms = wc_get_product_terms( $product_variations->get_id(), $attribute_name, array( 'fields' => 'all' ) );
																							
												$output .= '<select id="' . esc_attr( sanitize_title( $attribute_name ) ) . '" name="attribute_' . esc_attr( sanitize_title( $attribute_name ) ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute_name ) ) . '" data-show_option_none="yes">';

												$choose_option = __('Choose Option', 'wc-multi-select-product');
												$output .= '<option value="">'.$choose_option.'</option>';
												foreach ( $terms as $term ) { 
									                if ( in_array( $term->slug, $options ) ) { 
									                    $output .= '<option value="' . esc_attr( $term->slug ) . '" class="" termid="'.esc_attr( $term->term_id ).'">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</option>'; 
									                } 
												}
												$output .= '</select>';
												$output .='</td>';
												//var_dump($variations);
												$output .= '</tr>';
											}
											$clear = $attribute_name ? '<a class="reset_variations" href="#">' . __( 'Clear', 'wc-multi-select-product' ) . '</a>' : '';
											$output .= $clear;
										$output .= '</tbody>
										</table>
									</form>';
								}
					          	$output .= '<div class="wpcare-woocommerce-product-button">
					            <button class="button secondary expanded" style="'.$btn_background_color.$btn_font_size.$btn_font_color.$btn_padding.$btn_margin.'">';
					            	$uniqid_multi = 'multi-'.uniqid().'';
					              	$output .= '<input id="'.$uniqid_multi.'" type="checkbox" name="multi-product[]" value="'.$current_id.'" class="'.$is_composite.'">';
					              	$output .= '<label for="'.$uniqid_multi.'">'.__('Select Product', 'wc-multi-select-product').'</label>';
					              	$output .= '<span class="qty-here"></span>';
					            $output .='</button>';
					            $output .= '<a class="button secondary expanded add_to_cart_button product_type_'.$product->get_type().'" style="'.$btn_background_color.$btn_font_size.$btn_font_color.$btn_padding.$btn_margin.'" href="'.$product->add_to_cart_url().'" data-product_id="'.$current_id.'">'.$addtocart_title.'</a>';
					          	if(class_exists( 'YITH_WCQV' ) && !$product->is_type( 'composite' ) && $quickview == 'yes') { 
									$output .= '<a class="button secondary expanded yith-wcqv-button" style="'.$btn_background_color.$btn_font_size.$btn_font_color.$btn_padding.$btn_margin.'" href="#" data-toggle="modal" data-product_id="'.$current_id.'">'.__("Quick View", "wc-multi-select-product").'</a>';
					            }
					          	$output .='</div>'; //button

								if ($product->is_type( 'composite' )) {
									$output .= '<div class="composite-link" composite-link="'.get_the_permalink($current_id).'"></div>';
								}
								
					        $output .='</div>
					    </div>'; // column
					    
					}
				endwhile;

				if($linkto == 'cart') {
					$goto_cart = wc_get_cart_url();
					$goto_title = __('GO TO CART', 'wc-multi-select-product');
				}else{
					$goto_cart = wc_get_checkout_url();
					$goto_title = __('GO TO CHECKOUT', 'wc-multi-select-product');
				}
				
				
				

				$output .= '</div>'; //Wrapper

				$output .= '<br><div class="wpcareme-no-selected clearfix" style="'.$info_background_color.$info_font_size.$info_font_color.$info_padding.$info_margin.'">';
				$output .= '<div class="error">'.__("You Have Selected ", "wc-multi-select-product").' <span class="wcmsp-no-selected"><strong>0</strong></span> '.__("of", "wc-multi-select-product").' <span class="wcmsp-min-selection"><strong>'.$min_no.'</strong></span> '.__(" minimum needed to Continue Checkout", "wc-multi-select-product").'</div>';
				$output .= '<div class="multi-product-btn '.$uniqid.'"></div>';
				$output .= '<a href="'.$goto_cart.'" class="multi-gotocart hidden">'.$goto_title.'</a>';
				$output .= '</div>';

				$success_title = __('Products Added Successfully!', 'wc-multi-select-product');
				
				$output .= "<script>
				jQuery(document).ready(function($) {

					
					var goto_composite = '';

					//Show / Hide Button
					$('#{$uniqid} input:checkbox').on('click', function(e) {
						//map checked products ids
						var product_IDs = $('#{$uniqid} input:checkbox:checked').map(function(){
					      return $(this).val();
					    }).get();

						var checked_count = product_IDs.length;

						//map checked composite products
						var composite_checked = $('#{$uniqid} input.is_composite:checkbox:checked').map(function(){
					      return $(this).val();
					    }).get();
						
						var composite_checked_no = composite_checked.length;

						if (composite_checked_no > 1) {
							alert('Sorry you can\'t choose more than 1 composite product.');
							$(this).prop('checked', false);
						}

						//composite product link to go after successfull addition to cart
						goto_composite = $('#{$uniqid} input.is_composite:checkbox:checked').closest('.tt-wrapper').find('.composite-link').attr('composite-link');
												
						$('.wcmsp-no-selected').html('<strong>' + checked_count + '<strong>');

						//show / hide qty field						
						$('#{$uniqid} input:checkbox:checked').each(function(){
							$(this).on('change', function() { 
								if($(this).is(':checked')) {
									$(this).closest('.tt-wrapper').find('.qty-here').html('<input type=number min=1 class=wpcareme-qty name=multi-qty[] value=1 placeholder=Qty>');
									console.log(this);
								}else {
									$(this).closest('.tt-wrapper').find('.qty-here').html('');
									console.log(this);
								}
							});	
						});
					
						//show button if meeting minimum allowed products
						if (checked_count >= {$min_no}) {
							$('.multi-product-btn').html('<button class=button><strong>{$goto_title}</strong></button>');
						}else{
							$('.multi-product-btn').html('');
						}
					});
					
					//Add to Cart
				    $('.{$uniqid}.multi-product-btn').on('click', 'button', function(e) {

					    e.preventDefault();

					    var query = '';

					    $('#{$uniqid} input:checkbox:checked').each(function(){

					    	var product_IDs = $(this).val();

					    	var product_qty = $(this).closest('.tt-wrapper').find('.qty-here input').val();

					    	var item = {};

					    	var variations = $(this).closest('.tt-wrapper').find( 'select[name^=attribute]' );
					    	//loop through variations
					    	variations.each( function() {
					    		var this_variation = $( this );
					    		attributeName = this_variation.attr( 'name' );
								attributevalue = this_variation.val();

								if ( attributevalue.length === 0 ) {
									var index = attributeName.lastIndexOf( '_' );
									var attributeTaxName = attributeName.substring( index + 1 );
								} else {

									item[attributeName] = attributevalue;

								}
					    	});
							
							//map attributes and join them with |
							var list_attributes = $.map(item, function(value, name) {
							 	return name + '=' + value;
							}).join('|');
							
							//query to return url via ajax
							query += product_IDs + ':' + product_qty + '|' + list_attributes + ',';
					    	
							console.log(query);
					    });
			
						//Sending Data via ajax

					    $.ajax({
						  url: '{$goto_cart}' + '?add-to-cart=' + query,
						  type: 'get', //send it through get method

						  success: function(response) {
						    //Do Something
						    $('.{$uniqid}.multi-product-btn').html('{$success_title}');
						    $('#{$uniqid} input:checkbox:checked').attr('checked', false);
						    $('.multi-gotocart').show();
						    $('.qty-here').html('');
						    if(goto_composite) {
						    	location = goto_composite;
							}
						  },
						  error: function(xhr) {
						    //Do Something to handle error
						    alert('Error Occured, Retry Again.');
						  }
						});
				    

				    });

				});

			    </script>";

			    $output .= "<style type='text/css'>
			    	#{$uniqid} .button:hover {
			    		{$btn_hover_background_color}
			    		{$btn_hover_font_color}
			    		{$btn_hover_font_size}
			    		{$btn_hover_margin}
			    		{$btn_hover_padding}
			    	}
			    	.{$uniqid}.multi-product-btn button {
			    		{$btn_background_color_info}
			    		{$btn_font_size_info}
			    		{$btn_font_color_info}
			    		{$btn_padding_info}
			    		{$btn_margin_info}
			    	}
			    </style>";
			    
				// Reset Post Data
				wp_reset_postdata();
			}

			return $output;	
	    }
	}
}


new WPCAREME_Multi_Select_Products_ShortCode;



?>