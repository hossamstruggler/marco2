<?php 


if(!class_exists('WPCAREME_WC_MULTI_SELECT_PRODUCTS')){
    
    /**
     * Initiate Addons
     */
    class WPCAREME_WC_MULTI_SELECT_PRODUCTS {
        /**
         * Initiate Addons
         */

        public function __construct() {

            $options = get_option('wpcareme_wcmsp_settings');

            //Load Visual Composer Addon
            if (isset($options['wpcareme_wcmsp_visual_composer']) == 1) {
                require_once( WPCAREME_WCMSP_MODULES. 'tabbed_select_products.php');
                if(!function_exists('wpcareme_tabbed_select_products_func')){
                    $wpcareme_tabbed_select_products = new WPCAREME_TABBED_SELECT_PRODUCTS;
                }
            }
        	

            //Load Shortcodes
            if (isset($options['wpcareme_wcmsp_shortcode']) == 1) {
                require_once(WPCAREME_WCMSP_DIR . 'shortcodes/multi_select_products_shortcode.php');
            }
        }
    }
}


