<?php 
/*
Plugin Name: WooCommerce MultiSelect Product
Plugin URI: https://wpcare.me
Description: Select Multiple Products and add them all to the shopping cart.
Version: 1.0
Author: WPCare.me
Author URI: https://wpcare.me
*/

if ( !defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly


if ( !function_exists( 'is_plugin_active' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}


if ( !defined( 'WPCAREME_WCMSP_VERSION' ) ) {
    define( 'WPCAREME_WCMSP_VERSION', '1.0.0' );
}

if ( !defined( 'WPCAREME_WCMSP_FILE' ) ) {
    define( 'WPCAREME_WCMSP_FILE', __FILE__ );
}

if ( !defined( 'WPCAREME_WCMSP_DIR' ) ) {
    define( 'WPCAREME_WCMSP_DIR', plugin_dir_path( __FILE__ ) );
}

if ( !defined( 'WPCAREME_WCMSP_URL' ) ) {
    define( 'WPCAREME_WCMSP_URL', plugins_url( '/', __FILE__ ) );
}

if ( !defined( 'WPCAREME_WCMSP_ADMIN_PATH' ) ) {
    define( 'WPCAREME_WCMSP_ADMIN_PATH', WPCAREME_WCMSP_DIR . 'admin/' );
}

if ( !defined( 'WPCAREME_WCMSP_MODULES' ) ) {
    define( 'WPCAREME_WCMSP_MODULES', WPCAREME_WCMSP_DIR . 'modules/' );
}

function wpcareme_init_multi_select_product() {
	if ( !function_exists( 'WC' ) ) {
        add_action( 'admin_notices', 'wpcareme_wcmsp_woocommerce_install' );
    }

    if(!defined('WPB_VC_VERSION')){
        add_action('admin_notices', 'wpcareme_wcmsp_install_visual_composer');
        return;
    }
    // Load required classes and functions
    require_once( WPCAREME_WCMSP_DIR . 'wc-multi-select-product.php' );
    require_once( WPCAREME_WCMSP_DIR . 'params.php' );

    //Load Admin Settings
    require_once(WPCAREME_WCMSP_ADMIN_PATH . 'settings.php');

    add_image_size( 'wcmsp_default', 400, 400, true );  

	/* Load text domain */
    load_plugin_textdomain( 'wc-multi-select-product', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    
    new WPCAREME_WC_MULTI_SELECT_PRODUCTS;
}

add_action( 'plugins_loaded', 'wpcareme_init_multi_select_product', 11 );
add_action('init', 'wpcareme_admin_styling');

if (!function_exists('wpcareme_admin_styling')) {
    function wpcareme_admin_styling() {
        if(is_admin()){
            wp_register_style("tt-framework-select2-bootstrap", plugins_url("modules/assets/css/select2-bootstrap.css", __FILE__));
            wp_register_style("tt-framework-select2", plugins_url("modules/assets/css/select2.css", __FILE__));
            wp_register_style("tt-framework-backend-css", plugins_url("modules/assets/css/backend.min.css", __FILE__));
            wp_register_style("wpcare-app.css", plugins_url("modules/assets/css/app.css", __FILE__));

            wp_enqueue_style("tt-framework-select2-bootstrap");
            wp_enqueue_style("tt-framework-select2");
            wp_enqueue_style("tt-framework-backend-css");
            isset($_GET['page']) ? wp_enqueue_style("wpcare-app.css") : '';

            wp_register_script("tt-framework-select2-js", plugins_url("modules/assets/js/select2.js", __FILE__));

            wp_enqueue_script("tt-framework-select2-js");

            wp_register_script("tt-framework-backend-js", plugins_url("modules/assets/js/backend.min.js", __FILE__));

            wp_enqueue_script("tt-framework-backend-js");

        }else {
            wp_register_style("wpcare-frontend", plugins_url("modules/assets/css/wpcare-frontend.css", __FILE__));
            wp_register_style("wpcare-frontend-font", 'https://fonts.googleapis.com/css?family=Poppins:400,700');
            //wp_register_style("pretty-checkbox.css", plugins_url("modules/assets/css/pretty-checkbox.min.css", __FILE__));
        
            wp_enqueue_style("wpcare-frontend");
            wp_enqueue_style("wpcare-frontend-font");
            //wp_enqueue_style("pretty-checkbox.css");

            wp_register_script("foundation.js", 'http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js', '', '', true);

            wp_enqueue_script("foundation.js");
        }
    }
}
function wpcareme_wcmsp_woocommerce_install() {
    ?>
    <div class="error">
        <p><?php _e( 'WooCommerce Multi Select Product Addon is enabled but not effective. It requires WooCommerce in order to work.', 'wc-multi-select-product' ); ?></p>
    </div>
    <?php
}

function wpcareme_wcmsp_install_visual_composer() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
        <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="https://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'supreme-addons'), $plugin_data['Name']).'</p>
    </div>';
}

function wpcareme_add_multiple_products_to_cart( $url = false ) {
    // Make sure WC is installed, and add-to-cart qauery arg exists, and contains at least one comma.
    if ( ! class_exists( 'WC_Form_Handler' ) || empty( $_REQUEST['add-to-cart'] ) || false === strpos( $_REQUEST['add-to-cart'], ',' ) ) {
        return;
    }

    // Remove WooCommerce's hook, as it's useless (doesn't handle multiple products).
    remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'add_to_cart_action' ), 20 );

    $product_ids = explode( ',', $_REQUEST['add-to-cart'] );
    $count       = count( $product_ids );
    $number      = 0;

    // add-to-cart=61:2|attribute_pa_color=black|attribute_pa_size=l,81|attribute_pa_color=red|attribute_pa_size=l

    foreach ( $product_ids as $id_and_quantity ) {
        // Check for quantities defined in curie notation (<product_id>:<product_quantity>)
        $id_and_quantityx = explode('|', $id_and_quantity);

        $id_and_quantity = explode( ':', $id_and_quantityx[0] );

        $product_id = $id_and_quantity[0];

        $_REQUEST['quantity'] = ! empty( $id_and_quantity[1] ) ? absint( $id_and_quantity[1] ) : 1;

        if(count($id_and_quantityx) > 0){
            for($i = 1; $i <= count($id_and_quantityx); $i++){
                $xo = $id_and_quantityx[$i];
                $xox = explode('=', $xo);

                $_REQUEST[$xox[0]] = $xox[1];
            }
        }

        if ( ++$number === $count ) {
            // Ok, final item, let's send it back to woocommerce's add_to_cart_action method for handling.
            $_REQUEST['add-to-cart'] = $product_id;

            return WC_Form_Handler::add_to_cart_action( $url );
        }

        $product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product_id ) );

        $was_added_to_cart = false;
        $adding_to_cart    = wc_get_product( $product_id );

        if ( ! $adding_to_cart ) {
            continue;
        }

        $add_to_cart_handler = apply_filters( 'woocommerce_add_to_cart_handler', $adding_to_cart->get_type(), $adding_to_cart );

        // Variable product handling
        if ( 'variable' === $add_to_cart_handler ) {
            wpcareme_invoke_method( 'WC_Form_Handler', 'add_to_cart_handler_variable', $product_id );

        // Grouped Products
        } elseif ( 'grouped' === $add_to_cart_handler ) {
            wpcareme_invoke_method( 'WC_Form_Handler', 'add_to_cart_handler_grouped', $product_id );

        // Custom Handler
        } elseif ( has_action( 'woocommerce_add_to_cart_handler_' . $add_to_cart_handler ) ){
            do_action( 'woocommerce_add_to_cart_handler_' . $add_to_cart_handler, $url );

        // Simple Products
        } else {
            wpcareme_invoke_method( 'WC_Form_Handler', 'add_to_cart_handler_simple', $product_id );
        }
    }
    
}
// Fire before the WC_Form_Handler::add_to_cart_action callback.
add_action( 'wp_loaded', 'wpcareme_add_multiple_products_to_cart', 15 );


/**
 * Invoke class private method
 *
 * @since   0.1.0
 *
 * @param   string $class_name
 * @param   string $methodName
 *
 * @return  mixed
 */
function wpcareme_invoke_method( $class_name, $methodName ) {
    if ( version_compare( phpversion(), '5.3', '<' ) ) {
        throw new Exception( 'PHP version does not support ReflectionClass::setAccessible()', __LINE__ );
    }

    $args = func_get_args();
    unset( $args[0], $args[1] );
    $reflection = new ReflectionClass( $class_name );
    $method = $reflection->getMethod( $methodName );
    $method->setAccessible( true );

    $args = array_merge( array( $class_name ), $args );
    return call_user_func_array( array( $method, 'invoke' ), $args );
}


?>