<?php 


if(!class_exists('WPCAREME_TABBED_SELECT_PRODUCTS')){
	/**
	 * WPCAREME_TABBED_SELECT_PRODUCTS
	 */
	class WPCAREME_TABBED_SELECT_PRODUCTS {
	    /**
	     * WPCAREME_TABBED_SELECT_PRODUCTS
	     */
	    function __construct() {
	    	vc_map(array(
				"name" => __("Multi Select Product", 'wc-multi-select-product'),
				"description" => __("Add Customized Multi Select Product.", 'wc-multi-select-product'),
				"base" => "wpcareme_tabbed_select_products",
				"class" => "wpcareme_tabbed_select_products",
				"icon" => plugins_url('', __FILE__),
				"category" => __('WPCare', 'wc-multi-select-product'),
				"params" => array(
					array(
						'type' => 'el_id',
						'heading' => __('Unique ID', 'wc-multi-select-product'),
						'param_name' => 'uniqid',
						'value' => __('wcmsptsp-'.uniqid(), 'wc-multi-select-product'),
						'description' => __('Enter a unique id for your post list.', 'wc-multi-select-product')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Custom Class', 'wc-multi-select-product'),
						'param_name' => 'custom_class',
						'value' => __('', 'wc-multi-select-product'),
						'description' => __('Enter a custom class for your post list items.', 'wc-multi-select-product')
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Link To", "wc-multi-select-product"),
						"param_name" => "linkto",
						"value" =>  array(
							__("Cart","wc-multi-select-product") => "cart",
							__("Checkout","wc-multi-select-product") => "checkout",
						)
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Enable Multi Select ?", "wc-multi-select-product"),
						"param_name" => "enable_multi",
						"value" =>  array(
							__("Yes","wc-multi-select-product") => "yes",
							__("No","wc-multi-select-product") => "no",
						)	
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Filter By", "wc-multi-select-product"),
						"param_name" => "filter_by",
						"value" =>  array(
							__("No Filter","wc-multi-select-product") => "no",
							__("Include Categories","wc-multi-select-product") => "filter_inc_cats",
							__("Exclude Categories","wc-multi-select-product") => "filter_exc_cats",
							__("Include Products","wc-multi-select-product") => "filter_inc_prod",
							__("Exclude Products","wc-multi-select-product") => "filter_exc_prod",
						),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
						"type" => "wpcareme_wcmsp_products_cats",
						"class" => "",
						"heading" => __("Include Categories", 'wc-multi-select-product'),
						"param_name" => "inc_cats",
						"value" => '',
						"dependency" => Array("element" => "filter_by", "value" => array("filter_inc_cats")),
						"description" => __("Choose categories to display.", 'wc-multi-select-product'),
						"group" => __("Query", 'wc-multi-select-product')					
					),
					array(
						"type" => "wpcareme_wcmsp_products",
						"class" => "",
						"heading" => __("Include Products", 'wc-multi-select-product'),
						"param_name" => "inc_prod",
						"value" => '',
						"dependency" => Array("element" => "filter_by", "value" => array("filter_inc_prod")),
						"description" => __("Choose products to display.", 'wc-multi-select-product'),
						"group" => __("Query", 'wc-multi-select-product')					
					),
					array(
						"type" => "wpcareme_wcmsp_products_cats",
						"class" => "",
						"heading" => __("Exclude Categories", 'wc-multi-select-product'),
						"param_name" => "exc_cats",
						"value" => '',
						"dependency" => Array("element" => "filter_by", "value" => array("filter_exc_cats")),
						"description" => __("Choose categories to exclude, please use with Include Categories.", 'wc-multi-select-product'),
						'group' => __('Query', 'wc-multi-select-product')					
					),
					array(
						"type" => "wpcareme_wcmsp_products",
						"class" => "",
						"heading" => __("Exclude Products", 'wc-multi-select-product'),
						"param_name" => "exc_prod",
						"value" => '',
						"dependency" => Array("element" => "filter_by", "value" => array("filter_exc_prod")),
						"description" => __("Choose products to exclude.", 'wc-multi-select-product'),
						"group" => __("Query", 'wc-multi-select-product')					
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Number of Columns", "wc-multi-select-product"),
						"param_name" => "columns",
						"value" =>  array(
							__("One","wc-multi-select-product") => "1",
							__("Two","wc-multi-select-product") => "2",
							__("Three","wc-multi-select-product") => "3",
							__("Four","wc-multi-select-product") => "4",
						),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Products Per Page", "wc-multi-select-product"),
						"param_name" => "products_perpage_all",
						"value" =>  array(
							__("All","wc-multi-select-product") => -1,
							__("Custom","wc-multi-select-product") => "custom",
						),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Products Per Page', 'wc-multi-select-product'),
						'param_name' => 'products_perpage',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						"dependency" => Array("element" => "products_perpage_all", "value" => array("custom")),
						'description' => __('How many products you want to show per page?.', 'wc-multi-select-product'),					
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
						'type' => 'textfield',
						'heading' => __('Offset Posts', 'wc-multi-select-product'),
						'param_name' => 'offset',
						'value' => __('', 'wc-multi-select-product'),
						'group' => __('Query', 'wc-multi-select-product')
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Order", "wc-multi-select-product"),
						"param_name" => "order",
						"value" =>  array(
							__("ASC","wc-multi-select-product") => "ASC",
							__("DESC","wc-multi-select-product") => "DESC",
						),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => __("Order By", "wc-multi-select-product"),
                        "param_name" => "orderby",
                        "value" =>  array(
                            __("Date","wc-multi-select-product") => "date",
                            __("Title","wc-multi-select-product") => "title",
                            __("Name","wc-multi-select-product") => "name",
                            __("Author","wc-multi-select-product") => "author",
                            __("ID","wc-multi-select-product") => "ID",
                            __("Post Types","wc-multi-select-product") => "type",
                            __("Comments Count","wc-multi-select-product") => "comment_count",
                            __("Meta Value","wc-multi-select-product") => "meta_value",
                            __("Random","wc-multi-select-product") => "rand",
                        ),
                        "group" => __("Query", 'wc-multi-select-product')	
                    ),
                    array(
						"type" => "textfield",
						"class" => "",
						"heading" => __("Meta Key", 'wc-multi-select-product'),
						"param_name" => "meta_key",
						"value" => __("", 'wc-multi-select-product'),
						"dependency" => Array("element" => "orderby", "value" => array("meta_value")),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => __("Meta Value", 'wc-multi-select-product'),
						"param_name" => "meta_value",
						"value" => __("", 'wc-multi-select-product'),
						"dependency" => Array("element" => "orderby", "value" => array("meta_value")),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					/*array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Pagination", "wc-multi-select-product"),
						"param_name" => "pagination",
						"value" =>  array(
							__("Show","wc-multi-select-product") => "show",
							__("Hide","wc-multi-select-product") => "hide",
						),
						'group' => __('Query', 'wc-multi-select-product')
					),*/
					array(
						'type' => 'dropdown',
						'heading' => __('Set Minimum Product Limit?', 'wc-multi-select-product'),
						'param_name' => 'set_min_no',
						"value" =>  array(
							__("No","wc-multi-select-product") => "no",
							__("Yes","wc-multi-select-product") => "yes",
						),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Minimum No. to add to Cart', 'wc-multi-select-product'),
						'param_name' => 'min_no',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('', 'wc-multi-select-product'),
						"dependency" => Array("element" => "set_min_no", "value" => array("yes")),
						"description" => __("Ex: 3", 'wc-multi-select-product'),
						"group" => __("Query", 'wc-multi-select-product')	
					),
					//Media
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Image Size", "wc-multi-select-product"),
						"param_name" => "img_size",
						"value" =>  array(
							__("Fullsize","wc-multi-select-product") => "full",
							__("Default (400x400)","wc-multi-select-product") => "wcmsp_default",
							__("Medium","wc-multi-select-product") => "medium",
							__("Thumbnail","wc-multi-select-product") => "thumbnail",
							__("Custom Size","wc-multi-select-product") => "custom",
						),
						'group' => __('Media', 'wc-multi-select-product')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Custom Size', 'wc-multi-select-product'),
						'param_name' => 'custom_size',
						'value' => __('', 'wc-multi-select-product'),
						"description" => __("Ex: 400x400", 'wc-multi-select-product'),
						"dependency" => Array("element" => "img_size", "value" => array("custom")),
						'group' => __('Media', 'wc-multi-select-product')
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Img Links to", "wc-multi-select-product"),
						"param_name" => "img_linkto",
						"value" =>  array(
							__("No Where","wc-multi-select-product") => "no",
							__("Product","wc-multi-select-product") => "product",							
						),
						'group' => __('Media', 'wc-multi-select-product')
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Show Price", "wc-multi-select-product"),
						"param_name" => "show_price",
						"value" =>  array(
							__("Yes","wc-multi-select-product") => "yes",
							__("No","wc-multi-select-product") => "no",
						),
						'group' => __('Media', 'wc-multi-select-product')
					),
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Price Background Color", 'wc-multi-select-product'),
						"param_name" => "price_background_color",
						"value" => '',
						'group' => __('Media', 'wc-multi-select-product')
					),
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Price Font Color", 'wc-multi-select-product'),
						"param_name" => "price_font_color",
						"value" => '',
						'group' => __('Media', 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Price Font Size', 'wc-multi-select-product'),
						'param_name' => 'price_font_size',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 30", 'wc-multi-select-product'),
						'group' => __('Media', 'wc-multi-select-product')	
					),
					//Excerpt
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Show Excerpt", "wc-multi-select-product"),
						"param_name" => "show_excerpt",
						"value" =>  array(
							__("Yes","wc-multi-select-product") => "yes",
							__("No","wc-multi-select-product") => "no",
						),
						'group' => __('Excerpt', 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Excerpt Length', 'wc-multi-select-product'),
						'param_name' => 'excerpt_length',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('', 'wc-multi-select-product'),
						"description" => __("Insert number of words to show", 'wc-multi-select-product'),
						'group' => __('Excerpt', 'wc-multi-select-product')	
					),
					//Btn
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Background Color", 'wc-multi-select-product'),
						"param_name" => "btn_background_color",
						"value" => '',
						"group" => __("Button", 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Font size', 'wc-multi-select-product'),
						'param_name' => 'btn_font_size',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 14", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Font Color", 'wc-multi-select-product'),
						"param_name" => "btn_font_color",
						"value" => '',
						"group" => __("Button", 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Top', 'wc-multi-select-product'),
						'param_name' => 'btn_padding_top',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Right', 'wc-multi-select-product'),
						'param_name' => 'btn_padding_right',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Bottom', 'wc-multi-select-product'),
						'param_name' => 'btn_padding_bottom',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Left', 'wc-multi-select-product'),
						'param_name' => 'btn_padding_left',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Top', 'wc-multi-select-product'),
						'param_name' => 'btn_margin_top',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Right', 'wc-multi-select-product'),
						'param_name' => 'btn_margin_right',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Bottom', 'wc-multi-select-product'),
						'param_name' => 'btn_margin_bottom',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Left', 'wc-multi-select-product'),
						'param_name' => 'btn_margin_left',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Button", 'wc-multi-select-product')	
					),
					// Btn Hover
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Background Color", 'wc-multi-select-product'),
						"param_name" => "btn_hover_background_color",
						"value" => '',
						"group" => __("Hover Button", 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Font size', 'wc-multi-select-product'),
						'param_name' => 'btn_hover_font_size',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 14", 'wc-multi-select-product'),
						"group" => __("Hover Button", 'wc-multi-select-product')	
					),
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Font Color", 'wc-multi-select-product'),
						"param_name" => "btn_hover_font_color",
						"value" => '',
						"group" => __("Hover Button", 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding', 'wc-multi-select-product'),
						'param_name' => 'btn_hover_padding',
						'value' => __('', 'wc-multi-select-product'),
						'min' => 1,
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Hover Button", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin', 'wc-multi-select-product'),
						'param_name' => 'btn_hover_margin',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Hover Button", 'wc-multi-select-product')	
					),
					//Quickview
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Enable Quickview", "wc-multi-select-product"),
						"param_name" => "quickview",
						"value" =>  array(
							__("Yes","wc-multi-select-product") => "yes",
							__("No","wc-multi-select-product") => "no",
						),
						"group" => __("Quickview", 'wc-multi-select-product')	
					),

					//Info Box
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Background Color", 'wc-multi-select-product'),
						"param_name" => "info_background_color",
						"value" => '',
						"group" => __("Info Box", 'wc-multi-select-product')
					),
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Border Color", 'wc-multi-select-product'),
						"param_name" => "info_border_color",
						"value" => '',
						"group" => __("Info Box", 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Border Size', 'wc-multi-select-product'),
						'param_name' => 'info_border_size',
						'min' => 0,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 14", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Border Radius', 'wc-multi-select-product'),
						'param_name' => 'info_border_radius',
						'min' => 0,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 14", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Font size', 'wc-multi-select-product'),
						'param_name' => 'info_font_size',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 14", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						"type" => "colorpicker",
						"class" => "",
						"heading" => __("Font Color", 'wc-multi-select-product'),
						"param_name" => "info_font_color",
						"value" => '',
						"group" => __("Info Box", 'wc-multi-select-product')
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Top', 'wc-multi-select-product'),
						'param_name' => 'info_padding_top',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Bottom', 'wc-multi-select-product'),
						'param_name' => 'info_padding_bottom',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Left', 'wc-multi-select-product'),
						'param_name' => 'info_padding_left',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Padding Right', 'wc-multi-select-product'),
						'param_name' => 'info_padding_right',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Top', 'wc-multi-select-product'),
						'param_name' => 'info_margin_top',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Bottom', 'wc-multi-select-product'),
						'param_name' => 'info_margin_bottom',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Left', 'wc-multi-select-product'),
						'param_name' => 'info_margin_left',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
					array(
						'type' => 'wpcareme_wcmsp_number',
						'heading' => __('Margin Right', 'wc-multi-select-product'),
						'param_name' => 'info_margin_right',
						'min' => 1,
						'value' => __('', 'wc-multi-select-product'),
						'suffix' => __('px', 'wc-multi-select-product'),
						"description" => __("Ex: 5", 'wc-multi-select-product'),
						"group" => __("Info Box", 'wc-multi-select-product')	
					),
				)
			));
			add_shortcode('wpcareme_tabbed_select_products', array($this, 'wpcareme_tabbed_select_products_func'));
	    }

	    public function wpcareme_tabbed_select_products_func($atts, $content = null) {
	    	extract(shortcode_atts(array(

				"uniqid" => "",
				"custom_class" => "",
				"enable_multi" => "yes",
				"filter_by" => "no",
				"inc_cats" => "",
				"inc_prod" => "",
				"exc_cats" => "",
				"exc_prod" => "",
				"columns" => "3",
				"products_perpage_all" => -1,
				"products_perpage" => "",
				"offset" => "",
				"order" => 'DESC',
				"orderby" => 'date',
				"meta_key" => "",
				"meta_value" => "",
				"pagination"=> "hide",
				"set_min_no" => "no",
				"min_no" => "3",
				"quickview" => 'yes',
				"linkto" => 'cart',
				"show_excerpt" => 'yes',
				"excerpt_length" => '30',
				"img_size" => 'full',
				"custom_size" => '',
				"img_linkto" => 'no',
				"show_price" => 'yes',
				"price_background_color" => '',
				"price_font_color" => '',
				"price_font_size" => '',
				"btn_background_color" => '',
				"btn_font_size" => '',
				"btn_font_color" => '',
				"btn_padding_top" => '',
				"btn_padding_bottom" => '',
				"btn_padding_left" => '',
				"btn_padding_right" => '',
				"btn_margin_top" => '',
				"btn_margin_bottom" => '',
				"btn_margin_left" => '',
				"btn_margin_right" => '',
				"btn_hover_background_color" => '',
				"btn_hover_font_size" => '',
				"btn_hover_font_color" => '',
				"btn_hover_padding" => '',
				"btn_hover_margin" => '',
				"info_background_color" => '',
				"info_font_size" => '',
				"info_font_color" => '',
				"info_padding_top" => '',
				"info_padding_bottom" => '',
				"info_padding_right" => '',
				"info_padding_left" => '',
				"info_margin_top" => '',
				"info_margin_bottom" => '',
				"info_margin_left" => '',
				"info_margin_right" => '',
				"info_border_color" => '',
				"info_border_size" => '',
				"info_border_radius" => ''

				
			), $atts));

			$content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
	    	

	    	global $post, $paged, $wp_query;

	    	wp_register_style("wpcareme-wcmsp-main.css", plugins_url("assets/css/main.css", __FILE__));
	    	wp_enqueue_style("wpcareme-wcmsp-main.css");

	    	/// Include categories
			if ( isset( $atts['inc_cats'] ) ) {
				$inc_cats = explode( ',', $atts['inc_cats'] );
				$inc_cats = array_map( 'trim', $inc_cats );

			} else {
				$inc_cats = '';
			}

			// Include Products
			if ( isset( $atts['inc_prod'] ) ) {
				$inc_prod = explode( ',', $atts['inc_prod'] );
				$inc_prod = array_map( 'trim', $inc_prod );

			} else {
				$inc_prod = '';
			}
			// Exclude Categories
			if ( isset( $atts['exc_cats'] ) ) {
				$exc_cats = explode( ',', $atts['exc_cats'] );
				$exc_cats = array_map( 'trim', $exc_cats );
			} else {
				$exc_cats = array();
			}

			//Exclude Products
			if ( isset( $atts['exc_prod'] ) ) {
				$exc_prod = explode( ',', $atts['exc_prod'] );
				$exc_prod = array_map( 'trim', $exc_prod );

			} else {
				$exc_prod = '';
			}

			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$args = array(
			    'post_type' => 'product',
			    'post_status' => 'publish',
			    'posts_per_page' => $products_perpage ? $products_perpage : $products_perpage_all,
			    'offset' => $offset,
	      		'orderby' =>$orderby,
		    	'order' =>$order,
		    	'meta_key' => $meta_key,
		    	'meta_value' => $meta_value,
		    	//'paged' => $paged,
			);

			if($filter_by !== 'no') {

				if($filter_by == 'filter_inc_cats') {
					$args['tax_query'] = array(
						array(
							array(
								'taxonomy' => 'product_cat',
								'field'    => 'id',
								'terms'    => $inc_cats
							),
						)
					);
				}

				if($filter_by == 'filter_exc_cats') {
					$args['tax_query'] = array(
						array(
							array(
								'taxonomy' => 'product_cat',
								'field'    => 'id',
								'terms'    => $exc_cats,
								'operator' => 'NOT IN'
							),
						)
					);
				}


				if($filter_by == 'filter_inc_prod') {
					$args['post__in'] = $inc_prod;
				}

				if($filter_by == 'filter_exc_prod') {
					$args['post__not_in'] = $exc_prod;
				}
			}

			$price_background_color = ($price_background_color != '') ? "background-color: {$price_background_color};" : "";
			$price_font_color = ($price_font_color != '') ? "color: {$price_font_color};" : "";
			$price_font_size = ($price_font_size != '') ? "font-size: {$price_font_size}px;" : "";
			
			//info
			$btn_background_color_info = ($btn_background_color != '') ? "background-color: {$btn_background_color}!important;" : "";
			$btn_font_color_info = ($btn_font_color != '') ? "color: {$btn_font_color}!important;" : "";
			$btn_font_size_info = ($btn_font_size != '') ? "font-size: {$btn_font_size}px!important;" : "";
			$btn_margin_info_top = ($btn_margin_top != '') ? "margin-top: {$btn_margin_top}px!important;" : "";
			$btn_margin_info_bottom = ($btn_margin_bottom != '') ? "margin-bottom: {$btn_margin_bottom}px!important;" : "";
			$btn_margin_info_right = ($btn_margin_right != '') ? "margin-right: {$btn_margin_right}px!important;" : "";
			$btn_margin_info_left = ($btn_margin_left != '') ? "margin-left: {$btn_margin_left}px!important;" : "";
			
			$btn_padding_info_top = ($btn_padding_top != '') ? "padding-top: {$btn_padding_top}px!important;" : "";
			$btn_padding_info_bottom = ($btn_padding_bottom != '') ? "padding-bottom: {$btn_padding_bottom}px!important;" : "";
			$btn_padding_info_right = ($btn_padding_right != '') ? "padding-right: {$btn_padding_right}px!important;" : "";
			$btn_padding_info_left = ($btn_padding_left != '') ? "padding-left: {$btn_padding_left}px!important;" : "";


			//btn
			$btn_background_color = ($btn_background_color != '') ? "background-color: {$btn_background_color};" : "";
			$btn_font_color = ($btn_font_color != '') ? "color: {$btn_font_color};" : "";
			$btn_font_size = ($btn_font_size != '') ? "font-size: {$btn_font_size}px;" : "";
			$btn_margin_top = ($btn_margin_top != '') ? "margin-top: {$btn_margin_top}px;" : "";
			$btn_margin_bottom = ($btn_margin_bottom != '') ? "margin-bottom: {$btn_margin_bottom}px;" : "";
			$btn_margin_left = ($btn_margin_left != '') ? "margin-left: {$btn_margin_left}px;" : "";
			$btn_margin_right = ($btn_margin_right != '') ? "margin-right: {$btn_margin_right}px;" : "";

			$btn_padding_top = ($btn_padding_top != '') ? "padding-top: {$btn_padding_top}px;" : "";
			$btn_padding_bottom = ($btn_padding_bottom != '') ? "padding-bottom: {$btn_padding_bottom}px;" : "";
			$btn_padding_left = ($btn_padding_left != '') ? "padding-left: {$btn_padding_left}px;" : "";
			$btn_padding_right = ($btn_padding_right != '') ? "padding-right: {$btn_padding_right}px;" : "";

			//btn hover
			$btn_hover_background_color = ($btn_hover_background_color != '') ? "background-color: {$btn_hover_background_color}!important;" : "";
			$btn_hover_font_color = ($btn_hover_font_color != '') ? "color: {$btn_hover_font_color}!important;" : "";
			$btn_hover_font_size = ($btn_hover_font_size != '') ? "font-size: {$btn_hover_font_size}px!important;" : "";
			$btn_hover_margin = ($btn_hover_margin != '') ? "margin: {$btn_hover_margin}px!important;" : "";
			$btn_hover_padding = ($btn_hover_padding != '') ? "padding: {$btn_hover_padding}px!important;" : "";
			
			//Info Box
			$info_background_color = ($info_background_color != '') ? "background-color: {$info_background_color}!important;" : "";
			$info_border_color = ($info_border_color != '') ? "border-color: {$info_border_color}!important;" : "";
			$info_font_color = ($info_font_color != '') ? "color: {$info_font_color}!important;" : "";
			$info_font_size = ($info_font_size != '') ? "font-size: {$info_font_size}px!important;" : "";
			$info_border_size = ($info_border_size != '') ? "border-width: {$info_border_size}px!important;" : "";
			$info_border_radius = ($info_border_radius != '') ? "border-radius: {$info_border_radius}px!important;" : "";
			$info_margin_top = ($info_margin_top != '') ? "margin-top: {$info_margin_top}px;" : "";
			$info_margin_bottom = ($info_margin_bottom != '') ? "margin-bottom: {$info_margin_bottom}px;" : "";
			$info_margin_left = ($info_margin_left != '') ? "margin-left: {$info_margin_left}px;" : "";
			$info_margin_right = ($info_margin_right != '') ? "margin-right: {$info_margin_right}px;" : "";

			$info_padding_top = ($info_padding_top != '') ? "padding-top: {$info_padding_top}px;" : "";
			$info_padding_bottom = ($info_padding_bottom != '') ? "padding-bottom: {$info_padding_bottom}px;" : "";
			$info_padding_left = ($info_padding_left != '') ? "padding-left: {$info_padding_left}px;" : "";
			$info_padding_right = ($info_padding_right != '') ? "padding-right: {$info_padding_right}px;" : "";


			if($custom_size) {
				$img_size = explode( 'x', $custom_size );
				$img_size = implode(', ', $img_size);
			}

			if($set_min_no == 'no') {
				$min_no = '1';
			}
			
			$products_query = new WP_Query( $args );

			$output = '';

			if ($products_query->have_posts()) {

				$output .= '<div id="'.$uniqid.'" class="row small-up-1 large-up-'.$columns.' '.$custom_class.'">';
				while ($products_query->have_posts()) : 

					$products_query->the_post();

					$current_id = get_the_ID();

					$product = wc_get_product( $current_id );
					
					
					$product_variations = new WC_Product_Variable( $current_id );
					$variations = $product_variations->get_available_variations();
					$attributes = $product_variations->get_variation_attributes();
					
					if (!$product->is_type( 'grouped' )) {

						if ($product->is_type( 'composite' )) {
							$is_composite = 'is_composite';
						}else{
							$is_composite = '';
						}

						
						
					    $output .= '<div class="column tt-wrapper clearfix">';
					    	if($product->is_type( 'simple' )) {
								$addtocart_title = __('Add To Cart', 'wc-multi-select-product');
							}else{
								$addtocart_title = __('Select Options', 'wc-multi-select-product');
							}
					        $output .='<div class="wpcare-woocommerce-product-wrap">
					          <div class="wpcare-woocommerce-product-image">';
					          	if ($img_linkto == 'product'){
					          		$output .= '<a href="'.get_the_permalink().'">';
					          	}
					          	if (get_the_post_thumbnail_url($current_id)) {
					          		$output .= '<img class="thumbnail" src="'.get_the_post_thumbnail_url($current_id, $img_size).'">';
					          	}else {
					          		$output .= '<img class="thumbnail" src="http://placehold.it/400x400">';
					          	}
					          	if ($show_price == 'yes') {
					            	$output .= '<div class="wpcareme-wcmsp-price" style="'.$price_background_color.$price_font_color.$price_font_size.'">'.$product->get_price_html().'</div>';
					          	}
					          	if ($img_linkto == 'product'){
					          		$output .= '</a>';
					          	}
					          $output .='</div>
					          <div class="wpcare-woocommerce-product-title">
					            <h5>'. get_the_title() .'</h5>
					          </div>';
					          	if($show_excerpt == 'yes') {
						          	$output .='<div class="wpcare-woocommerce-product-description">
						          	<p>'.wp_trim_words( get_the_content(), $excerpt_length, '' ).'</p>
						          	</div>
						          	<br>';
						      	}
					          	if ($variations && !$product->is_type( 'composite' ) && $enable_multi == 'yes') {
									$variationIds = array();
									foreach ($variations as $key => $variation) {

		                                array_push($variationIds, $variation['variation_id']);

									}
									$index = 0;
									$output .= "<form class='variations_form cart' method='post' enctype='multipart/form-data' data-product_id='{$current_id}' data-product_variations='".json_encode($variations)."'>";

										$output .= '<table class="variations">
										<tbody>';

											
											foreach ($attributes as $attribute_name => $options) {

												
												//$meta = get_post_meta($variationIds[$index], 'attribute_'.$attribute_name, true);
												
												$output .= '<tr>';
												$output .= '<td class="label">' . wc_attribute_label( $attribute_name ) . '</td>';
												$output .= '<td class="value">';
												$terms = wc_get_product_terms( $product_variations->get_id(), $attribute_name, array( 'fields' => 'all' ) );
																							
												$output .= '<select id="' . esc_attr( sanitize_title( $attribute_name ) ) . '" name="attribute_' . esc_attr( sanitize_title( $attribute_name ) ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute_name ) ) . '" data-show_option_none="yes">';

												$choose_option = __('Choose Option', 'wc-multi-select-product');
												$output .= '<option value="">'.$choose_option.'</option>';
												foreach ( $terms as $term ) { 
									                if ( in_array( $term->slug, $options ) ) { 
									                    $output .= '<option value="' . esc_attr( $term->slug ) . '" class="" termid="'.esc_attr( $term->term_id ).'">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</option>'; 
									                } 
												}
												$output .= '</select>';
												$output .='</td>';
												//var_dump($variations);
												$output .= '</tr>';
											}
											$clear = $attribute_name ? '<a class="reset_variations" href="#">' . __( 'Clear', 'wc-multi-select-product' ) . '</a>' : '';
											$output .= $clear;
										$output .= '</tbody>
										</table>
									</form>
									<br>';
								}
					          	$output .= '<div class="wpcare-woocommerce-product-button">';
					          	if($enable_multi == 'yes') {
						            $output .= '<a class="button secondary expanded select-prod-btn" style="'.$btn_background_color.$btn_font_size.$btn_font_color.$btn_padding_top.$btn_padding_bottom.$btn_padding_right.$btn_padding_left.$btn_margin_top.$btn_margin_bottom.$btn_margin_right.$btn_margin_left.'">';
						            	$uniqid_multi = 'multi-'.uniqid().'';
						              	$output .= '<input id="'.$uniqid_multi.'" type="checkbox" name="multi-product[]" value="'.$current_id.'" class="'.$is_composite.'">';
						              	$output .= '<span for="'.$uniqid_multi.'">'.__('Select Product', 'wc-multi-select-product').'</span>';
						              	$output .= '<span class="qty-here"></span>';
						            $output .='</a>';
					        	}
					            if($enable_multi == 'no') {
					            	$output .= '<a class="button secondary expanded add_to_cart_button product_type_'.$product->get_type().'" style="'.$btn_background_color.$btn_font_size.$btn_font_color.$btn_padding_top.$btn_padding_bottom.$btn_padding_right.$btn_padding_left.$btn_margin_top.$btn_margin_bottom.$btn_margin_right.$btn_margin_left.'" href="'.$product->add_to_cart_url().'" data-product_id="'.$current_id.'">'.$addtocart_title.'</a>';
					          	}
					          	if(class_exists( 'YITH_WCQV' ) && !$product->is_type( 'composite' ) && $quickview == 'yes') { 
									$output .= '<a class="button secondary expanded yith-wcqv-button" style="'.$btn_background_color.$btn_font_size.$btn_font_color.$btn_padding_top.$btn_padding_bottom.$btn_padding_right.$btn_padding_left.$btn_margin_top.$btn_margin_bottom.$btn_margin_right.$btn_margin_left.'" href="#" data-toggle="modal" data-product_id="'.$current_id.'">'.__("Quick View", "wc-multi-select-product").'</a>';
					            }
					          	$output .='</div>'; //button

								if ($product->is_type( 'composite' )) {
									$output .= '<div class="composite-link" composite-link="'.get_the_permalink($current_id).'"></div>';
								}
								
					        $output .='</div>
					    </div>'; // column
					    
					}

				endwhile;
				


				if($linkto == 'cart') {
					$goto_cart = wc_get_cart_url();
					$goto_title = __('GO TO CART', 'wc-multi-select-product');
				}else{
					$goto_cart = wc_get_checkout_url();
					$goto_title = __('GO TO CHECKOUT', 'wc-multi-select-product');
				}
				
				
				

				$output .= '</div>';

				if($enable_multi == 'yes') {

					
					$output .= '<br><div class="wpcareme-no-selected clearfix" style="'.$info_background_color.$info_padding_top.$info_padding_bottom.$info_padding_right.$info_padding_left.$info_margin_top.$info_margin_bottom.$info_margin_right.$info_margin_left.$info_border_color.$info_border_size.$info_border_radius.'">';
					$output .= '<div class="error" style="'.$info_font_size.$info_font_color.'"><p>'.__("You Have Selected ", "wc-multi-select-product").' <span class="wcmsp-no-selected"><strong>0</strong></span>';
					if($set_min_no == 'yes') {
						$output .= ' '.__("of", "wc-multi-select-product").' <span class="wcmsp-min-selection"><strong>'.$min_no.'</strong></span> '.__(" minimum needed to Continue Checkout", "wc-multi-select-product").'</p></div>';
					}else {
						$output .= __("Products to add to cart.", "wc-multi-select-product"). '</p></div>';
					}
					$output .= '<div class="multi-product-btn '.$uniqid.'"></div>';
					//$output .= '<a href="'.$goto_cart.'" class="multi-gotocart hidden">'.$goto_title.'</a>';
					$output .= '</div>';

					$success_title = __('Products Added Successfully!', 'wc-multi-select-product');
					$sorry_msg = __("Sorry you can\'t choose more than 1 composite product.", 'wc-multi-select-product');
					
					$output .= "<script>
					jQuery(document).ready(function($) {

						
						var goto_composite = '';

						//Show / Hide Button
						$('#{$uniqid} input:checkbox').on('click', function(e) {
							//map checked products ids
							var product_IDs = $('#{$uniqid} input:checkbox:checked').map(function(){
						      return $(this).val();
						    }).get();

							var checked_count = product_IDs.length;

							//map checked composite products
							var composite_checked = $('#{$uniqid} input.is_composite:checkbox:checked').map(function(){
						      return $(this).val();
						    }).get();
							
							var composite_checked_no = composite_checked.length;

							if (composite_checked_no > 1) {
								alert('{$sorry_msg}');
								$(this).prop('checked', false);
							}

							//composite product link to go after successfull addition to cart
							goto_composite = $('#{$uniqid} input.is_composite:checkbox:checked').closest('.tt-wrapper').find('.composite-link').attr('composite-link');
													
							$('.wcmsp-no-selected').html('<strong>' + checked_count + '<strong>');

							//show / hide qty field						
							$('#{$uniqid} input:checkbox:checked').each(function(){
								$(this).on('change', function() { 
									if($(this).is(':checked')) {
										$(this).closest('.tt-wrapper').find('.qty-here').html('<input type=number min=1 class=wpcareme-qty name=multi-qty[] value=1 placeholder=Qty>');
										console.log(this);
									}else {
										$(this).closest('.tt-wrapper').find('.qty-here').html('');
										console.log(this);
									}
								});	
							});
						
							//show button if meeting minimum allowed products
							if (checked_count >= {$min_no}) {
								$('.multi-product-btn').html('<button class=button><strong>{$goto_title}</strong></button>');
							}else{
								$('.multi-product-btn').html('');
							}
						});
						
						//Add to Cart
					    $('.{$uniqid}.multi-product-btn').on('click', 'button', function(e) {

						    e.preventDefault();

						    var query = '';

						    $('#{$uniqid} input:checkbox:checked').each(function(){

						    	var product_IDs = $(this).val();

						    	var product_qty = $(this).closest('.tt-wrapper').find('.qty-here input').val();

						    	var item = {};

						    	var variations = $(this).closest('.tt-wrapper').find( 'select[name^=attribute]' );
						    	//loop through variations
						    	variations.each( function() {
						    		var this_variation = $( this );
						    		attributeName = this_variation.attr( 'name' );
									attributevalue = this_variation.val();

									if ( attributevalue.length === 0 ) {
										var index = attributeName.lastIndexOf( '_' );
										var attributeTaxName = attributeName.substring( index + 1 );
									} else {

										item[attributeName] = attributevalue;

									}
						    	});
								
								//map attributes and join them with |
								var list_attributes = $.map(item, function(value, name) {
								 	return name + '=' + value;
								}).join('|');
								
								//query to return url via ajax
								query += product_IDs + ':' + product_qty + '|' + list_attributes + ',';
						    	
								console.log(query);
						    });
				
							//Sending Data via ajax

						    $.ajax({
							  url: '{$goto_cart}' + '?add-to-cart=' + query,
							  type: 'get', //send it through get method

							  success: function(response) {
							    //Do Something
							    $('.{$uniqid}.multi-product-btn').html('{$success_title}');
							    $('#{$uniqid} input:checkbox:checked').attr('checked', false);
							    $('.multi-gotocart').show();
							    $('.qty-here').html('');
							    if(goto_composite) {
							    	location = goto_composite;
								}else {
									location = '{$goto_cart}';
								}
							  },
							  error: function(xhr) {
							    //Do Something to handle error
							    alert('Error Occured, Retry Again.');
							  }
							});
					    

					    });

					});

				    </script>";
			    }
			    $output .= "<style type='text/css'>
			    	#{$uniqid} .button:hover {
			    		{$btn_hover_background_color}
			    		{$btn_hover_font_color}
			    		{$btn_hover_font_size}
			    		{$btn_hover_margin}
			    		{$btn_hover_padding}
			    	}
			    	.{$uniqid}.multi-product-btn button {
			    		{$btn_background_color_info}
			    		{$btn_font_size_info}
			    		{$btn_font_color_info}
			    		{$btn_padding_info_top}
			    		{$btn_padding_info_bottom}
			    		{$btn_padding_info_left}
			    		{$btn_padding_info_right}
			    		{$btn_margin_info_top}
			    		{$btn_margin_info_bottom}
			    		{$btn_margin_info_left}
			    		{$btn_margin_info_right}
			    	}
			    </style>";
			    
				// Reset Post Data
				wp_reset_postdata();
			}

			return $output;
	    }

	    
	}
}

?>