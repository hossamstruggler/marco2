<?php

if(defined('WPB_VC_VERSION') && version_compare(WPB_VC_VERSION, 4.8) >= 0) {
	if(function_exists('vc_add_shortcode_param'))
	{
		vc_add_shortcode_param('wpcareme_wcmsp_products_cats', 'wpcareme_wcmsp_products_categories');
	}
}
else {
	if(function_exists('add_shortcode_param')) {
		add_shortcode_param('wpcareme_wcmsp_products_cats', 'wpcareme_wcmsp_products_categories');
	}
}

function wpcareme_wcmsp_products_categories($settings, $value){
	$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
	$type = isset($settings['type']) ? $settings['type'] : '';
	$class = isset($settings['class']) ? $settings['class'] : '';
	$wpcareme_wcmsp_products_cats = get_terms( 'product_cat', 'orderby=count&hide_empty=0' );
	$output = $selected = $ids = '';
	if ( $value !== '' ) {
		$ids = explode( ',', $value );
		$ids = array_map( 'trim', $ids );
	} else {
		$ids = array();
	}
	$output .= '<select class="sel2_cat sel2_cat_'.$param_name.'" multiple="multiple" style="width:100%;">';
	foreach($wpcareme_wcmsp_products_cats as $cat){
		if(in_array($cat->term_id, $ids)){
			$selected = 'selected="selected"';
		} else {
			$selected = '';
		}
		$output .= '<option '.$selected.' value="'.$cat->term_id.'">'. __($cat->name,'wpcareme_wcmsp') .'</option>';
	}
	$output .= '</select>';

	$output .= "<input type='hidden' name='".$param_name."' value='".$value."' class='wpb_vc_param_value sel_cat_{$param_name} ".$param_name." ".$type." ".$class."'>";
	$output .= '<script type="text/javascript">
					jQuery("select.sel2_cat.sel2_cat_'.$param_name.'").select2({
						placeholder: "Select Categories",
						allowClear: true
					});
					jQuery("select.sel2_cat.sel2_cat_'.$param_name.'").on("change",function(){
						jQuery(".sel_cat_'.$param_name.'").val(jQuery(this).val());
					});
				</script>';
	return $output;

} /* end wpcareme_wcmsp_products_categories*/

if(defined('WPB_VC_VERSION') && version_compare(WPB_VC_VERSION, 4.8) >= 0) {
	if(function_exists('vc_add_shortcode_param'))
	{
		vc_add_shortcode_param('wpcareme_wcmsp_products', 'wpcareme_wcmsp_products');
	}
}
else {
	if(function_exists('add_shortcode_param')) {
		add_shortcode_param('wpcareme_wcmsp_products', 'wpcareme_wcmsp_products');
	}
}

function wpcareme_wcmsp_products($settings, $value){
	$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
	$type = isset($settings['type']) ? $settings['type'] : '';
	$class = isset($settings['class']) ? $settings['class'] : '';
	$wpcareme_wcmsp_products = get_posts( 'post_type=product' );
	$output = $selected = $ids = '';
	if ( $value !== '' ) {
		$ids = explode( ',', $value );
		$ids = array_map( 'trim', $ids );
	} else {
		$ids = array();
	}
	$output .= '<select class="sel2_prod sel2_prod_'.$param_name.'" multiple="multiple" style="width:100%;">';
	foreach($wpcareme_wcmsp_products as $prod){
		if(in_array($prod->term_id, $ids)){
			$selected = 'selected="selected"';
		} else {
			$selected = '';
		}
		$output .= '<option '.$selected.' value="'.$prod->ID.'">'. __($prod->post_title,'wpcareme_wcmsp') .'</option>';
	}
	$output .= '</select>';

	$output .= "<input type='hidden' name='".$param_name."' value='".$value."' class='wpb_vc_param_value sel_prod_{$param_name} ".$param_name." ".$type." ".$class."'>";
	$output .= '<script type="text/javascript">
					jQuery("select.sel2_prod.sel2_prod_'.$param_name.'").select2({
						placeholder: "Select Categories",
						allowClear: true
					});
					jQuery("select.sel2_prod.sel2_prod_'.$param_name.'").on("change",function(){
						jQuery(".sel_prod_'.$param_name.'").val(jQuery(this).val());
					});
				</script>';
	return $output;

} /* end wpcareme_wcmsp_products*/

/* wpcareme_wcmsp_products_types */

if(defined('WPB_VC_VERSION') && version_compare(WPB_VC_VERSION, 4.8) >= 0) {
	if(function_exists('vc_add_shortcode_param'))
	{
		vc_add_shortcode_param('wpcareme_wcmsp_products_types', 'wpcareme_wcmsp_products_types');
	}
}
else {
	if(function_exists('add_shortcode_param')) {
		add_shortcode_param('wpcareme_wcmsp_products_types', 'wpcareme_wcmsp_products_types');
	}
}

function wpcareme_wcmsp_products_types($settings, $value){
	$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
	$type = isset($settings['type']) ? $settings['type'] : '';
	$class = isset($settings['class']) ? $settings['class'] : '';
	$product_types = get_products_types( '', 'objects' );

	$output = $selected = $ids = '';
	if ( $value !== '' ) {
		$ids = explode( ',', $value );
		$ids = array_map( 'trim', $ids );
	} else {
		$ids = array();
	}
	$output .= '<select class="sel2_posttypes sel2_posttypes_'.$param_name.'" multiple="multiple" style="width:100%;">';
	foreach($product_types as $product_type){
		if(in_array($product_type->name, $ids)){
			$selected = 'selected="selected"';
		} else {
			$selected = '';
		}
		$output .= '<option '.$selected.' value="'.$product_type->name.'">'. __($product_type->name,'wpcareme_wcmsp') .'</option>';
	}
	$output .= '</select>';

	$output .= "<input type='hidden' name='".$param_name."' value='".$value."' class='wpb_vc_param_value sel_posttypes_{$param_name} ".$param_name." ".$type." ".$class."'>";
	$output .= '<script type="text/javascript">
					jQuery("select.sel2_posttypes.sel2_posttypes_'.$param_name.'").select2({
						placeholder: "Select Categories",
						allowClear: true
					});
					jQuery("select.sel2_posttypes.sel2_posttypes_'.$param_name.'").on("change",function(){
						jQuery(".sel_posttypes_'.$param_name.'").val(jQuery(this).val());
					});
				</script>';
	return $output;

} 
/* end wpcareme_wcmsp_products_types */

/// Number Param

if(!class_exists('wpcareme_wcmsp_Number_Param'))
{
	class wpcareme_wcmsp_Number_Param
	{
		function __construct()
		{
			if(defined('WPB_VC_VERSION') && version_compare(WPB_VC_VERSION, 4.8) >= 0) {
				if(function_exists('vc_add_shortcode_param'))
				{
					vc_add_shortcode_param('wpcareme_wcmsp_number' , array(&$this, 'wpcareme_wcmsp_number_settings_field' ));
				}
			}
			else {
				if(function_exists('add_shortcode_param'))
				{
					add_shortcode_param('wpcareme_wcmsp_number' , array(&$this, 'wpcareme_wcmsp_number_settings_field' ));
				}
			}
		}

		function wpcareme_wcmsp_number_settings_field($settings, $value)
		{
			
			$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type = isset($settings['type']) ? $settings['type'] : '';
			$min = isset($settings['min']) ? $settings['min'] : '';
			$max = isset($settings['max']) ? $settings['max'] : '';
			$step = isset($settings['step']) ? $settings['step'] : '';
			$suffix = isset($settings['suffix']) ? $settings['suffix'] : '';
			$class = isset($settings['class']) ? $settings['class'] : '';
			$output = '<input type="number" min="'.$min.'" max="'.$max.'" step="'.$step.'" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="'.$value.'" style="max-width:100px; margin-right: 10px;" />'.$suffix;
			return $output;
		}

	}
}

if(class_exists('wpcareme_wcmsp_Number_Param'))
{
	$wpcareme_wcmsp_Number_Param = new wpcareme_wcmsp_Number_Param();
}

// End of wpcareme_wcmsp_number


//// Margin/Paddings Param

if(!class_exists('wpcareme_wcmsp_Margin_Param'))
{
	class wpcareme_wcmsp_Margin_Param
	{
		function __construct()
		{
			if(defined('WPB_VC_VERSION') && version_compare(WPB_VC_VERSION, 4.8) >= 0) {
				if(function_exists('vc_add_shortcode_param'))
				{
					vc_add_shortcode_param('wpcareme_wcmsp_margins', array($this, 'wpcareme_wcmsp_margins_param'));
				}
			}
			else {
				if(function_exists('add_shortcode_param'))
				{
					add_shortcode_param('wpcareme_wcmsp_margins', array($this, 'wpcareme_wcmsp_margins_param'));
				}
			}
		}

		function wpcareme_wcmsp_margins_param($settings, $value)
		{
			
			$positions = $settings['positions'];
			$html = '<div class="tt-margins">
						<input type="hidden" name="'.$settings['param_name'].'" class="wpb_vc_param_value tt-margin-value '.$settings['param_name'].' '.$settings['type'].'_field" value="'.$value.'"/>';
					foreach($positions as $key => $position)
						$html .= $key.' <input type="text" style="width:50px;padding:3px" data-hmargin="'.$position.'" class="tt-margin-inputs" id="margin-'.$key.'" /> &nbsp;&nbsp;';
			$html .= '</div>';
			return $html;
		}

	}
}

if(class_exists('wpcareme_wcmsp_Margin_Param'))
{
	$wpcareme_wcmsp_Margin_Param = new wpcareme_wcmsp_Margin_Param();
}