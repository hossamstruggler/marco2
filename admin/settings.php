<?php

function wpcareme_wcmsp_do_settings_sections($page) {

    global $wp_settings_sections, $wp_settings_fields;

    if ( !isset($wp_settings_sections) || !isset($wp_settings_sections[$page]) )
        return;

    foreach( (array) $wp_settings_sections[$page] as $section ) {
        //echo "<h3>{$section['title']}</h3>\n";
        call_user_func($section['callback'], $section);
        if ( !isset($wp_settings_fields) ||
             !isset($wp_settings_fields[$page]) ||
             !isset($wp_settings_fields[$page][$section['id']]) )
                continue;
        echo '<div class="settings-form-wrapper">';
        wpcareme_wcmsp_do_settings_fields($page, $section['id']);
        echo '</div>';
    }
}

function wpcareme_wcmsp_do_settings_fields($page, $section) {
    global $wp_settings_fields;

    if ( !isset($wp_settings_fields) ||
         !isset($wp_settings_fields[$page]) ||
         !isset($wp_settings_fields[$page][$section]) )
        return;

    foreach ( (array) $wp_settings_fields[$page][$section] as $field ) {
        echo '<div class="settings-form-row">';
        if ( !empty($field['args']['label_for']) )
            //echo '<p><label for="' . $field['args']['label_for'] . '">' .
                $field['title'] . '</label><br />';
        else
            //echo '<p>' . $field['title'] . '<br />';
        call_user_func($field['callback'], $field['args']);
        echo '</div>';
    }
}

add_option('wpcareme_wcmsp_settings', array(
	'wpcareme_wcmsp_visual_composer' => 1,
	'wpcareme_wcmsp_shortcode' => 1,
));

add_action('admin_menu', 'wpcareme_wcmsp_add_admin_menu');
add_action('admin_init', 'wpcareme_wcmsp_settings_init');

function wpcareme_wcmsp_add_admin_menu(){
	add_menu_page('WC Multi Select Products', 'WC Multi Select Products', 'manage_options', 'wcmsp_addons', 'wpcareme_wcmsp_options_page', null, 99);
	add_submenu_page( 'wcmsp_addons', 'Our Plugins', 'Our Plugins', 'manage_options', 'wcmsp_addons_free', 'wpcareme_wcmsp_settings_page' );
	add_submenu_page( 'wcmsp_addons', 'Premium Features', 'Premium Features', 'manage_options', 'wcmsp_addons_premium', 'wpcareme_wcmsp_premium_page' );
}

function wpcareme_wcmsp_settings_init(){
	register_setting('wcmsp', 'wpcareme_wcmsp_settings');

	add_settings_section(
		'wpcareme_wcmsp_wcmsp_section', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_settings_section_callback', 
		'wcmsp'
	);

	add_settings_field(
		'wpcareme_wcmsp_visual_composer', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_visualcomposer_render', 
		'wcmsp', 
		'wpcareme_wcmsp_wcmsp_section' 
	);
	add_settings_field(
		'wpcareme_wcmsp_shortcode', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_wpshortcode_render', 
		'wcmsp', 
		'wpcareme_wcmsp_wcmsp_section' 
	);



	register_setting('wcmsp_settings', 'wpcareme_wcmsp_settings_2');

	add_settings_section(
		'wpcareme_wcmsp_wcmsp_global_settings', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_settings_global_settings_callback', 
		'wcmsp_settings'
	);

	add_settings_field(
		'wpcareme_wcmsp_free_install', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_free_render', 
		'wcmsp_settings', 
		'wpcareme_wcmsp_wcmsp_global_settings' 
	);


	register_setting('wcmsp_premium', 'wpcareme_wcmsp_settings_3');

	add_settings_section(
		'wpcareme_wcmsp_premium_features', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_settings_premium_callback', 
		'wcmsp_premium'
	);

	add_settings_field(
		'wpcareme_wcmsp_premium', 
		__('', 'wc-multi-select-product'), 
		'wpcareme_wcmsp_premium_render', 
		'wcmsp_premium', 
		'wpcareme_wcmsp_premium_features' 
	);
}

function wpcareme_wcmsp_visualcomposer_render(){
	$options = get_option('wpcareme_wcmsp_settings');
	?>
  	<tr>
      	<td>Visual Composer Addon</td>
      	<td>
        	<div class="switch small">
		          <input class="switch-input" id="wpcareme_visual_composer" type="checkbox" name="wpcareme_wcmsp_settings[wpcareme_wcmsp_visual_composer]" <?php checked(is_array($options) && isset($options['wpcareme_wcmsp_visual_composer']), 1, true); ?> value='1'>
		          <label class="switch-paddle" for="wpcareme_visual_composer">
			          <span class="show-for-sr">Activation</span>
			          <span class="switch-active" aria-hidden="true">Yes</span>
			          <span class="switch-inactive" aria-hidden="true">No</span>
		          </label>
        	</div>
     	</td>
    </tr>
	<?php
}

function wpcareme_wcmsp_wpshortcode_render(){
	$options = get_option('wpcareme_wcmsp_settings');
	?>
  	<tr>
      	<td>WordPress Shortcode</td>
      	<td>
        	<div class="switch small">
		          <input class="switch-input" id="wpcareme_wp_shortcode" type="checkbox" name="wpcareme_wcmsp_settings[wpcareme_wcmsp_shortcode]" <?php checked(is_array($options) && isset($options['wpcareme_wcmsp_shortcode']), 1, true); ?> value='1'>
		          <label class="switch-paddle" for="wpcareme_wp_shortcode">
			          <span class="show-for-sr">Activation</span>
			          <span class="switch-active" aria-hidden="true">Yes</span>
			          <span class="switch-inactive" aria-hidden="true">No</span>
		          </label>
        	</div>
     	</td>
    </tr>
	<?php
}


function wpcareme_wcmsp_free_render(){

	$response = wp_remote_get( 'https://wpcloud.me/wp-json/wpcareme/v1/plugins' );

	if( is_wp_error( $response ) ) {
		return;
	}	

	$plugins = json_decode( wp_remote_retrieve_body( $response ) );

	if( empty( $plugins ) ) {
		return;
	}


	if( !empty( $plugins ) ) {
		foreach( $plugins as $plugin ) {
		echo '<div class="large-6 columns">
		        <div class="wpcareme_wcmsp_box clearfix '.$plugin->comming_soon.'">
		          <div class="row wpcareme_wcmsp_item">
		            <div class="large-4 columns tt_item"><img src="'.$plugin->img.'"></div>
		            <div class="large-8 columns tt_item">
		                <h5>'.$plugin->title.'</h5>
		                '.$plugin->description.'
		                <div class="item_switch clearfix">
		                  <span class="tt_label float-left">Like it?</span>
		                  <span class="float-right plugin-action-buttons">';
		                  if ($plugin->comming_soon == 'comming') {
		                  	echo '<p>'.__("Comming Soon", "wc-multi-select-product").'</p>';
		                  }else {
		                  	echo '<a class="install-now button open-plugin-details-modal thickbox" style="background:#0BA1D8;border:none" href="' . esc_url( network_admin_url('plugin-install.php?tab=plugin-information&plugin=' . $plugin->wp_install_id . '&TB_iframe=true&width=600&height=550' ) ) . '">'.__("Install", "wc-multi-select-product").'</a>';
		                  }
		                    
		                  echo '</span>
		                </div>
		            </div>
		          </div>
		        </div>
		  	</div>';
		}
	}
}

function wpcareme_wcmsp_premium_render(){

	$response = wp_remote_get( 'https://wpcloud.me/wp-json/wpcareme/v1/premium' );

	if( is_wp_error( $response ) ) {
		return;
	}	

	$plugins = json_decode( wp_remote_retrieve_body( $response ) );

	if( empty( $plugins ) ) {
		return;
	}


	if( !empty( $plugins ) ) {
		foreach( $plugins as $plugin ) {
		echo '<div class="large-6 columns">
		        <div class="wpcareme_wcmsp_box clearfix '.$plugin->comming_soon.'">
		          <div class="row wpcareme_wcmsp_item">
		            <div class="large-4 columns tt_item"><img src="'.$plugin->img.'"></div>
		            <div class="large-8 columns tt_item">
		                <h5>'.$plugin->title.'</h5>
		                '.$plugin->description.'
		                <div class="item_switch clearfix">
		                  <span class="tt_label float-left">Like it?</span>
		                  <span class="float-right plugin-action-buttons">';
		                  if ($plugin->comming_soon == 'comming') {
		                  	echo '<p>Comming Soon</p>';
		                  }else {
		                  	echo '<a class="install-now button open-plugin-details-modal" style="background:#0BA1D8;border:none" href="'.$plugin->wp_install_id.'">Details</a>';
		                  }
		                    
		                  echo '</span>
		                </div>
		                <h5>'.$plugin->price.'</h5>
		            </div>
		          </div>
		        </div>
		  	</div>';
		}
	}
}
function wpcareme_wcmsp_settings_section_callback(){
	echo __('', 'wc-multi-select-product');
}

function wpcareme_wcmsp_settings_global_settings_callback(){
	echo __('', 'wc-multi-select-product');
}

function wpcareme_wcmsp_settings_premium_callback(){
	echo __('', 'wc-multi-select-product');
}

function wpcareme_wcmsp_options_page(){
	?>
	<div class="row" style="padding-right: 1rem;">
	  <div class="medium-12" id="tt_top_bar">
	    <div class="top-bar">
	      <div class="top-bar-left">
	        <ul class="dropdown menu" data-dropdown-menu>
	          <li class="menu-text"><img src="<?php echo plugins_url('/img/logo.png', __FILE__); ?>" title="ThunderThemes.net"></li>
	        </ul>
	      </div>
	      <div class="top-bar-right">
	        <ul class="menu horizontal">
	            <li><a href="#">Support</a></li>
	            <li><a href="#">Documentation</a></li>
	            <li><a href="#">Video Tutorials</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row collapse" id="wpcareme_wcmsp_admin">
		<div class="tabs-content vertical clearfix" data-tabs-content="vertical-tabs">
			<div class="medium-2 columns" style="padding-left:0px;">
			    <ul class="tabs vertical" id="vertical-tabs" data-tabs>
			      <li class="tabs-title is-active"><a href="admin.php?page=wcmsp_addons" aria-selected="true">General Settings</a></li>
			      <li class="tabs-title"><a href="admin.php?page=wcmsp_addons_free">Our Plugins</a></li>
			      <li class="tabs-title"><a href="admin.php?page=wcmsp_addons_premium">Premium Features</a></li>
			    </ul>
			</div>
			<div class="medium-10 columns clearfix">
		      <div class="tabs-panel is-active" id="addons">
		        <div class="row">
		        	<div class="" id="settings">
						<div class="title">Global Settings</div>
							<form action='options.php' method='post'>
								<table>
							      	<thead>
								        <tr>
								          <th width="80%">Name</th>
								          <th>Activated?</th>
								        </tr>
							      	</thead>
							      		<tbody>	
											<?php
												$classes = array ('tt_save_button');
												settings_fields('wcmsp', 'wpcareme_wcmsp_wcmsp_section');
												wpcareme_wcmsp_do_settings_sections('wcmsp');		
											?>						
										</tbody>
						    	</table>
						    	<?php submit_button(__('Save Settings'), $classes); ?>
							</form>
						</div>
					</div>
				</div>
		      </div>
		  	</div>
	  	</div>
	</div>

	<?php
}

function wpcareme_wcmsp_settings_page(){ ?>
	<div class="row" style="padding-right: 1rem;">
	  <div class="medium-12" id="tt_top_bar">
	    <div class="top-bar">
	      <div class="top-bar-left">
	        <ul class="dropdown menu" data-dropdown-menu>
	          <li class="menu-text"><img src="<?php echo plugins_url('/img/logo.png', __FILE__); ?>" title="ThunderThemes.net"></li>
	        </ul>
	      </div>
	      <div class="top-bar-right">
	        <ul class="menu horizontal">
	            <li><a href="#">Support</a></li>
	            <li><a href="#">Documentation</a></li>
	            <li><a href="#">Video Tutorials</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row collapse" id="wpcareme_wcmsp_admin">
		<div class="tabs-content vertical clearfix" data-tabs-content="vertical-tabs">
			<div class="medium-2 columns" style="padding-left:0px;">
			    <ul class="tabs vertical" id="vertical-tabs" data-tabs>
			      <li class="tabs-title"><a href="admin.php?page=wcmsp_addons">General Settings</a></li>
			      <li class="tabs-title is-active"><a href="admin.php?page=wcmsp_addons_free" aria-selected="true">Our Plugins</a></li>
			      <li class="tabs-title"><a href="admin.php?page=wcmsp_addons_premium">Premium Features</a></li>
			    </ul>
			</div>
			<div class="medium-10 columns clearfix">
		      <div class="tabs-panel is-active" id="addons">
		        <div class="row">
					<div class="" id="settings">
					
					    
				        <form action='options.php' method='post'>
							<?php
								settings_fields('wcmsp_settings', 'wpcareme_wcmsp_wcmsp_global_settings');
								wpcareme_wcmsp_do_settings_sections('wcmsp_settings');							
							?>

						</form>
				    </div>
		      </div>
			</div>
		  	</div>
		</div>
	</div>
<?php }

function wpcareme_wcmsp_premium_page(){ ?>
	<div class="row" style="padding-right: 1rem;">
	  <div class="medium-12" id="tt_top_bar">
	    <div class="top-bar">
	      <div class="top-bar-left">
	        <ul class="dropdown menu" data-dropdown-menu>
	          <li class="menu-text"><img src="<?php echo plugins_url('/img/logo.png', __FILE__); ?>" title="ThunderThemes.net"></li>
	        </ul>
	      </div>
	      <div class="top-bar-right">
	        <ul class="menu horizontal">
	            <li><a href="#">Support</a></li>
	            <li><a href="#">Documentation</a></li>
	            <li><a href="#">Video Tutorials</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row collapse" id="wpcareme_wcmsp_admin">
		<div class="tabs-content vertical clearfix" data-tabs-content="vertical-tabs">
			<div class="medium-2 columns" style="padding-left:0px;">
			    <ul class="tabs vertical" id="vertical-tabs" data-tabs>
			      <li class="tabs-title"><a href="admin.php?page=wcmsp_addons">General Settings</a></li>
			      <li class="tabs-title"><a href="admin.php?page=wcmsp_addons_free">Our Plugins</a></li>
			      <li class="tabs-title is-active"><a href="admin.php?page=wcmsp_addons_premium" aria-selected="true">Premium Features</a></li>
			    </ul>
			</div>
			<div class="medium-10 columns clearfix">
		      <div class="tabs-panel is-active" id="addons">
		        <div class="row">
					<div class="" id="settings">
					
					    
				        <form action='options.php' method='post'>
							<?php
								settings_fields('wcmsp_premium', 'wpcareme_wcmsp_premium_features');
								wpcareme_wcmsp_do_settings_sections('wcmsp_premium');							
							?>

						</form>
				    </div>
		      </div>
			</div>
		  	</div>
		</div>
	</div>
<?php }