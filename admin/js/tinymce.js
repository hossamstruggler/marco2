(function() {
    tinymce.PluginManager.add('wcaddcustomdata', function( editor, url ) {
        editor.addButton( 'wcaddcustomdata', {
            title: 'TT ShortCodes',
            type: 'menubutton',
            icon: 'wp_code',
            menu: [
               {
                    text: 'Sale CountDown',
                    value: 'Sale CountDown',
                    icon: false,
                    onclick: function() {
                      editor.windowManager.open( {
                          title: 'CountDown',
                          body: [{
                              type: 'listbox',
                              name: 'type',
                              label: 'Show In',
                              'values': [
                                  {text: 'On Sale Products', value: 'all-in-sale'},
                                  {text: 'Certain Products', value: 'certain-products'}
                              ]
                            },
                            {
                                type: 'textbox',
                                name: 'product_ids',
                                label: 'Product IDs (comma seprated)'
                            },
                            {
                              type: 'listbox',
                              name: 'style',
                              label: 'Style',
                                'values': [
                                  {text: 'Style 1', value: 'style1'},
                                  {text: 'Style 2', value: 'style2'},
                                  {text: 'Style 3', value: 'style3'},
                                  {text: 'Style 4', value: 'style4'},
                                  {text: 'Style 5', value: 'style5'},
                                  {text: 'Style 6', value: 'style6'},
                                  {text: 'Style 7', value: 'style7'}
                                ]
                            }],
                          onsubmit: function( e ) { //e.data.level
                              editor.insertContent('[sale-count-down type="'+e.data.type+'" product_ids="'+e.data.product_ids+'" style="'+e.data.style+'"]');
                          }
                      });
                  }
               },
               {
                    text: 'Stock Bar',
                    value: 'Stock Bar',
                    icon: false,
                    onclick: function() {
                      editor.windowManager.open( {
                          title: 'Stock Progress Bar',
                          body: [{
                              type: 'listbox',
                              name: 'type',
                              label: 'Show In',
                              'values': [
                                  {text: 'On Sale Products', value: 'all-in-sale'},
                                  {text: 'Certain Products', value: 'certain-products'},
                                  {text: 'All Products', value: 'all-products'}
                              ]
                            },
                            {
                                type: 'textbox',
                                name: 'product_ids',
                                label: 'Product IDs (comma seprated)'
                            },
                            {
                              type: 'listbox',
                              name: 'style',
                              label: 'Style',
                                'values': [
                                  {text: 'Normal', value: 'normal'},
                                  {text: 'Striped', value: 'striped'},
                                  {text: 'Striped 2', value: 'progress-bar-striped'},
                                  {text: 'Animated Striped', value: 'animated-striped'}
                                ]
                            },
                            {
                                type: 'colorbox',
                                name: 'color',
                                label: 'Color'
                            }],
                          onsubmit: function( e ) { //e.data.level
                              editor.insertContent('[sales-bar type="'+e.data.type+'" product_ids="'+e.data.product_ids+'" style="'+e.data.style+'" color="'+e.data.color+'"]');
                          }
                      });
                  }
               },
           ]
        });
    });
})();